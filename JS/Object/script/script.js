// 1. Создать объект point, содержащий два свойства: "x" и "y" — координаты точки, и метод GetQuadrant, вычисляющий,
// в каком квадранте в декартовой системе координат находится данная точка.
// Протестируйте данный объект, изменяя значения его координат, и повторно вызавая метод GetQuadrant.

let sideX = +prompt("Input x");
let sideY = +prompt("Input y");


const point = {
    x: sideX,
    y: sideY,
    GetQuadrant: function(){
        if(point.x > 0 && point.y > 0){
            document.write("Point in right upper quadrant");
        }else if (point.x > 0 && point.y < 0){
            document.write ("Point in right lower quadrant");
        }else if (point.x < 0 && point.y < 0){
            document.write ("Point in left lower quadrant");
        }else{
            document.write("Point in left upper quadrant");
        }
    }
};
point.GetQuadrant();

// Создать объект Calculator, который должен содержать методы для расчета суммы, разности, произведения и частного двух чисел.
//        Протестировать данный объект, принимая от пользователя значения двух операндов и знак операции.
//        В зависимости от принятого знака операции, вызвать соответствующий метод.


const calculator = {
    firstNumber: 0,
    secondNumber: 0,
    sign: "",
    calculate: function(){
        switch(this.sign){
            case "+":
               return this.firstNumber + this.secondNumber;
            case "-":
               return this.firstNumber - this.secondNumber;
            case "*":
               return this.firstNumber * this.secondNumber;
            case "/":
                if(this.secondNumber == 0){
                    return Infinity;
                } else{
               return this.firstNumber / this.secondNumber;   
                }
            default: ('You can use only +, -, *, / sign');
            } 
        }
    }


    calculator.firstNumber = parseInt(prompt('Input first number'));
    calculator.secondNumber = parseInt(prompt('Input second number'));
    calculator.sign = prompt('Input sign');

    document.write(calculator.calculate());


//Создать массив, который будет хранить в себе информацию о сотрудниках предприятия. Каждый элемент масива — объект,
// содержащий свойства: name, sName, age, occupation, и метод show, который выводит всю информацию о пользователе.
// Реализовать заполнение массива пользователем. По окончанию заполнения — вывести информацию о сотрудниках.
// Для предыдущего задания создайте функцию, которая будет принимать в себя массив объектов-сотрудников, и каждому из объектов
//        будет добавлять новое свойство "salary", хранящее зарплату сотрудника. 
//        Зарплата расчитывается, исходя из значения свойства "occupation" следующим образом:
//            • "director" — 3000;
//            • "manager" — 1500;
//            • "programmer" — 2000;
//            • для остальных значений — 1000.
//        После выполнения функции — вывести информацию о сотрудниках.


let count = +prompt("Input quantity of employers");
//let occ = prompt('Input occupation');

let arrayOfEmployee = [count];
for (let index = 0; index < count; index++) {

arrayOfEmployee[index] = {
    name: prompt('Input name'),
    sName: prompt('Input  surname'),
    age: parseInt(prompt('Input age')),
    occupation: prompt('Input occupation'),
    show: function () {
        document.write(`<br><br>Name: <b>${this.name}</b><br>Last name: <b>${this.sName}</b><br>Age: <b>${this.age}</b><br>Occupation: <b>${this.occupation}</b><br>Salary: <b>${this.salary}`);
    }

     }
     putSalary(arrayOfEmployee);
     arrayOfEmployee[index].show();
}
function putSalary(array){
    for(i = 0; i < array.length; i++ ){
        switch(array[i].occupation){
        case "director":
        array[i].salary = 3000;
        continue;
        case "manager":
        array[i].salary = 1500;
        continue;
        case "programmer":
        array[i].salary = 2000;
        continue;
        default: array[i].salary = 1000;
        break;
        }
       
    }
    
    
}





// Для задания 3 создать метод, позволяющий отсортировать массив сотрудников по одному из свойств: name, sName, age, occupation, salary.
//        Параметр для сортировки принимается от пользователя.
//        После выполнения функции — вывести информацию о сотрудниках.

const sortingParameter = prompt('Chosee parameter for sorting - name, sName, age, occupation, salary.');

arrayOfEmployee.sort(function(a, b){
    switch(sortingParameter){
        case "name": 
        if(a.name > b.name){
            return 1;
        }
        if(a.name < b.name){
            return -1;
        }
        return 0;

        case "sName":
            if(a.sName > b.sName){
                return 1;
            }
            if(a.sName < b.sName){
                return -1;
            }
            return 0;

        case "age":
            
            return a.age - b.age;
        
        case "occupation":
                if(a.occupation > b.occupation){
                    return 1;
                }
                if(a.occupation < b.occupation){
                    return -1;
                }
                return 0;
        case "salary":
                    return a.salary - b.salary;
    
    }
});


