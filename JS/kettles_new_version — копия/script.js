var swiper = new Swiper('.swiper', {
    
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    effect: 'fade',
    clickable: true,
  
    // If we need pagination
    // pagination: {
    //   el: '.swiper-pagination',
    // },
  
    // Navigation arrows
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',

    // },
    
    // Navigation with mouse & keyboard
    mousewhell: true,
    keyboard: true,
  
    // And if we need scrollbar
    // scrollbar: {
    //   el: '.swiper-scrollbar',
    // },
  });

  // Свайп іконками чайників
  const bigKettles = document.querySelectorAll('.bigImage__kettle');
  
  const tiffany = document.querySelectorAll('.tiffany');
  const red = document.querySelectorAll('.red');
  const pink = document.querySelectorAll('.pink');
  const beige = document.querySelectorAll('.beige');
 

  
for (let i = 0; i < tiffany.length; i++){
  tiffany[i].addEventListener('click', ()=>{
    swiper.slideTo(0)
  });
}
for (let i = 0; i < red.length; i++){
  red[i].addEventListener('click', ()=>{
    swiper.slideTo(1)
  });
}
for (let i = 0; i < pink.length; i++){
  pink[i].addEventListener('click', ()=>{
    swiper.slideTo(2)
  });
}
for (let i = 0; i < beige.length; i++){
  beige[i].addEventListener('click', ()=>{
    swiper.slideTo(3)
  });
};




// Модальне вікно на десктопному екрані
const myModal = document.querySelector('.modalWindow');
console.log(myModal);

const modalLink = document.querySelectorAll('.nav__links');
for(let i = 0; i < modalLink.length; i++){
modalLink[i].addEventListener('click', function(){
  myModal.style.display = "block";

})};
myModal.addEventListener('click', function(){
  myModal.style.display = "none";
})

// Бургер меню
//Відкривається основне меню по кліку на кнопку
const button = document.querySelector('.burger__icon');
const navigation = document.querySelector('.root__nav--navigation');
button.addEventListener('click', ()=>{
  navigation.classList.toggle("root__nav--navigation--show")});
  

//Зовнішнє меню
const acc = document.getElementsByClassName("accordion");
const panels = document.querySelectorAll(".main-panel");

for (let i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    this.classList.toggle("active");
    const parent = this.parentElement;
    const panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
        parent.style.maxHeight =
        parseInt(parent.style.maxHeight) + panel.scrollHeight + "px";      
    }
  });  
}

//Внутрішнє меню
const accordion = document.querySelectorAll('.content-container');
for(let i = 0; i < accordion.length; i++){
    accordion[i].addEventListener('click',function(){
        this.classList.toggle('active');
    })
};

// TABS - працює не корректно. 
const tabButton = document.querySelectorAll('.accordion');
const tabContent = document.querySelectorAll('.main-panel');
for(i = 0; i < tabButton.length; i++){
    tabButton[i].addEventListener('click', showContent);
}

function showContent () {
    for(i = 0; i < tabButton.length; i++){
    tabButton[i].classList.remove('active');
    tabContent[i].style.display = "hide";
}
this.classList.add('active');
for(i = 0; i < tabButton.length; i++){
    if(tabButton[0].classList.contains('active') && tabButton[1].classList.contains("active") ){
      tabContent[0].style.maxHeight = null;
    }
    if(tabButton[1].classList.contains('active') && tabButton[2].classList.contains('active') ){
      tabContent[1].style.maxHeight = null;
    }
    if(tabButton[2].classList.contains('active') && tabButton[0].classList.contains('active') ){
      tabContent[2].style.maxHeight= null;
    }
  }
}









